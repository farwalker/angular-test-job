export class AppComponent {

  static componentName:string = "msApp";
  static componentConfig:ng.IComponentOptions = {
    bindings: {},
    controller: AppComponent,
    templateUrl: 'src/components/app/app.component.html'
  };

  private _date1:String;
  get date1() { return this._date1; };
  set date1(d) { this._date1 = d ? moment(d).format('YYYY-MM-DD') : null; }

  private _date2:String;
  get date2() { return this._date2; };
  set date2(d) { this._date2 = d ? moment(d).format('YYYY-MM-DD') : null; }

  constructor() {
    this._date1 = null;
    this._date2 = null;
  }

  changeDates() {
    setTimeout(() => {
	  let message:String[]|String = [];
	  if (this.date1) message.push(`С ${this.date1}`);
	  if (this.date2) message.push(`ПО ${this.date2}`);
	  message = message.length ? message.join(' ') : 'ДАТЫ НЕ УКАЗАНЫ';
      alert(message); }, 0);
  }
}
