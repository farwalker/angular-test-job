import 'angular-material';
import 'angular-messages';

export class McDatesComponent {

	static componentName:string = 'mcDates';
	static componentConfig:ng.IComponentOptions = {
		bindings: {
			dateFrom: '=',
			dateTo: '=',
			changeDates: '&mcChange'
		},
		controller: McDatesComponent,
		templateUrl: 'src/components/mc-dates/mc-dates.component.html'
	};

	constructor() {};

	preset(name:String) {
		let newFrom = this.dateFrom, newTo = this.dateTo;
		switch (name) {
			case 'yesterday':
				newFrom = newTo = moment(new Date()).add(-1, 'days');
				break;
			case 'today':
				newFrom = newTo = moment(new Date());
				break;
			case '2weeks':
				newFrom = moment(new Date());
				newTo = moment(new Date()).add(2, 'weeks');
				break;
			case 'month':
				newFrom = moment(new Date());
				newTo = moment(new Date()).add(1, 'months');
				break;
			case 'full':
				newFrom = newTo = null;
				break;
			default: break;
		}
		if (this.dateFrom !== newFrom.format('YYYY-MM-DD') ||
			this.dateTo !== newTo.format('YYYY-MM-DD')) this.changeDates();
		this.dateFrom = newFrom;
		this.dateTo = newTo;
	}

	changed(edge) {
		let err = false;
		if (this.dateFrom && this.dateTo) {
			if (moment(this.dateFrom) > moment(this.dateTo) && edge === 'from') {
				this.dateFrom = this.dateTo;
				err = true;
			}
			if (moment(this.dateTo) < moment(this.dateFrom) && edge === 'to') {
				this.dateTo = this.dateFrom;
				err = true;
			}
		}
		if (err) alert('Неверный диапазон дат!');
		this.changeDates();
	}
}