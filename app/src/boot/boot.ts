import * as angular from 'angular';
import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-moment';
import {AppComponent} from "../components/app/app.component.ts";
import {McDatesComponent} from "../components/mc-dates/mc-dates.component.ts";

module DatesApp {
  "use strict";
  angular
    .module('DatesApp', ['ngMaterial', 'angularMoment'])
    .config(function ($mdDateLocaleProvider:angular.material.IDateLocaleProvider) {
        $mdDateLocaleProvider.formatDate = function(date) {
		    return date ? moment(date).format('YYYY-MM-DD') : '';
        };
		$mdDateLocaleProvider.parseDate = function (dateString) {
			return dateString ? moment(dateString) : null;
		};
    })
    .controller("AppCtrl", function ($log) {})
    .component(AppComponent.componentName, AppComponent.componentConfig)
    .component(McDatesComponent.componentName, McDatesComponent.componentConfig);
}