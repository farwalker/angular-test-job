System.config({
  transpiler: 'typescript',
  defaultJSExtensions: false,
  baseURL: '/app/src',
  map: {
    'angular': '../node_modules/angular/angular.js',
    'angular-animate': '../node_modules/angular-animate/angular-animate.js',
    'angular-aria': '../node_modules/angular-aria/angular-aria.js',
    'angular-material': '../node_modules/angular-material/angular-material.js',
    'angular-messages': '../node_modules/angular-messages/angular-messages.js',
    'angular-sanitize': '../node_modules/angular-sanitize/angular-sanitize.js',
    'moment': '../node_modules/moment/moment.js',
    'angular-moment': '../node_modules/angular-moment/angular-moment.js'
  },
  
  meta: {
    'angular': { format: 'global' },
    'angular-animate': { format: 'global' },
    'angular-aria': { format: 'global' },
    'angular-material': { format: 'global' },
    'angular-messages': { format: 'global' },
    'angular-sanitize': { format: 'global' },
    'moment': { format: 'global' }
  }
});